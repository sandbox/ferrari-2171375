
README.txt
==========

********************************************************************
This is i18n menu admin 7.x, and will work with Drupal 7.x
********************************************************************
WARNING: DO READ THE INSTALL FILE AND the ON-LINE HANDBOOK
********************************************************************

This is an enhancement module for i18n menu.
This module allows to separate the menu items for languages.
The menu items will never mixed :-)

Additional Support
=================
For support, please create a support request for this module's project:
http://drupal.org/project/i18n_menu_admin

Support questions by email to the module maintainer will be simply ignored.
Use the issue tracker.

Now if you want professional (paid) support the module maintainer
may be available occasionally.
Drop me a message to check availability and hourly rates,
http://taller.net.br


====================================================================
Helal Ferrari Cabral, drupal at reyero dot net, http://www.taller.net.br/
